const mongoose = require('mongoose');

// Schema
const attributeSchema = new mongoose.Schema({
  id: { type: String, unique: true, required: true },
  label: { type: String, required: true },
  type: { type: String, required: true },
  // codeList: [{
  //   id: { type: String, required: true },
  //   codes: [{
  //     id: { type: String, required: true },
  //     value: { type: String, required: true },
  //   }],
  // }],
}, { _id: true });

attributeSchema.add({ structure: [attributeSchema] });

const schema = mongoose.Schema({
  id: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  agencyId: { type: String, required: true },
  structure: [attributeSchema],
}, { _id: true });

// Model
const model = module.exports = mongoose.model('metadatastructure', schema);

module.exports.get = function (callback, limit) {
  model.find(callback).limit(limit);
}
