require('dotenv').config();

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');

// middlewares
app.use(cors());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/** Database Connection */
const mongoose = require('mongoose');
const Metadata = require('./lib/Metadata');

mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, {
  auth: { 'authSource': 'admin' },
  user: process.env.DB_USERNAME,
  pass: process.env.DB_PASSWORD,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const db = mongoose.connection;

if (!db)
  console.log('Error connecting db');
else
  console.log('Db connected successfully');

/** Applications */
const routes = require('./routes');
const ctx = {
  db,
  metadata: new Metadata(db),
};

routes(app, ctx);

/** Start Server */
const port = parseInt(process.env.HTTP_PORT, 10) || 8092;

app.listen(port, (err) => {
  if (err) throw err;
  console.log(`> Running on http://localhost:${port}.`);
});
