const moment = require('moment');

const Metadataset = require('./model/metadata/Dataset');
const DataflowMapping = require('./model/metadata/DataflowMapping');

module.exports = function (app, { db, metadata }) {
  app.get('/', (req, res) => {
    res.send('OK');
  });

  app.get('/all', (req, res) => {
    metadata.all().then((docs) => {
      res.send(docs.map(({ id, name, agencyId }) => ({ id, name, agencyId })));
    });
  });

  app.get('/structure/:id', async (req, res) => {
    res.send(await metadata.findById(req.params.id));
  });

  app.get('/data/:id', async (req, res) => {
    res.send(await Metadataset.findOne({ msd: req.params.id }));
  });

  app.get('/dataByDataflow/:id', async (req, res) => {
    const mapping = await DataflowMapping.findOne({ dataflowId: req.params.id });
    const metadataset = await Metadataset.findOne({ msd: mapping.msdId });

    res.send(metadataset || { metadataset: [] });
  });

  app.get('/map/:id', async (req, res) => {
    res.send(await DataflowMapping.findOne({ dataflowId: req.params.id }));
  });

  app.post('/map/:id', async (req, res) => {
    const { body, params } = req;
    const dataflowId = params.id;
    const data = {
      dataflowId,
      msdId: body.msdId,
    };

    DataflowMapping.findOneAndUpdate({ dataflowId }, data, { upsert: true }, (err, doc) => {
      if (err) return res.send(500, { error: err });
      return res.send({ status: 'OK' });
    });
  });

  app.post('/save/:id', async (req, res) => {
    const { body, params } = req;
    const msd = await metadata.findById(params.id);

    const recMetadataset = (structures, values = {}) => {
      const result = [];

      structures.forEach(({ id, label, type, codeList = [], structure = [] }) => {
        if (values[id] !== undefined) {
          const attribute = { id, label, type };

          if (structure.length > 0) {
            attribute.child = recMetadataset(structure, values);
          }

          switch (type) {
            case 'DateTime':
              attribute.value = moment(values[id]);
              break;

            case 'enumeration':
              attribute.value = values[id];
              break;

            default:
              attribute.value = values[id];
              break;
          }

          result.push(attribute);
        }
      });

      return result;
    };

    Metadataset.findOneAndUpdate(
      { msd: params.id },
      {
        msd: params.id,
        metadataset: recMetadataset(msd.structure, body.content),
      },
      { upsert: true },
      (err) => {
        if (err) {
          res.send({ status: 'failed' });
        }
        else {
          res.send({ status: 'OK' });
        }
      }
    );
  });
};
