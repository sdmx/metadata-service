const mongoose = require('mongoose');

// Schema
const metadataset = new mongoose.Schema({
  id: { type: String, unique: true, required: true },
  label: { type: String, required: true },
  type: { type: String, required: true },
  value: { type: String, required: true },
}, { _id: true });

metadataset.add({ child: [metadataset] });

const schema = mongoose.Schema({
  msd: { type: String, required: true },
  metadataset: [metadataset],
});

// Model
const model = module.exports = mongoose.model('metadataset', schema);
