const fs = require('fs');
const libxml = require('libxmljs');

const MetadataStructure = require('../model/metadata/Structure');

const prefixes = {
  'str': 'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure',
  'com': 'http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common',
};

const Metadata = function(db) {
  this._db = db;
};

// FIXME: call sdmx registry api
Metadata.prototype.request = function () {
  return fs.readFileSync('./docs/msd-res-mini.xml').toString();
}

Metadata.prototype.all = function() {
  return new Promise((resolve, reject) => {
    // fetch msd from database
    MetadataStructure.find().exec((err, docs) => {
      if (!err) {
        // FIXME: call sdmx registry api
        const results = this.parse(this.request());

        MetadataStructure.collection.insertMany(results, (err, docs) => {
          if (err)
            console.info('Failed to stored data.');
          else
            console.info('Data were successfully stored.');
        });

        resolve(results);
      }
      else {
        resolve(docs);
      }
    });
  });
};

Metadata.prototype.findById = function(id) {
  return new Promise((resolve, reject) => {
    MetadataStructure.findOne({ id }).exec((err, docs) => {
      if (err)
        reject(err);
      else
        resolve(docs);
    });
  });
}

Metadata.prototype.parse = function(xml) {
  const doc = libxml.parseXmlString(xml);
  const msds = doc.find('//str:MetadataStructures/str:MetadataStructure', prefixes);
  const list = [];

  msds.forEach((msdDoc) => {
    const msd = {
      id: msdDoc.attr('id').value(),
      agencyId: msdDoc.attr('agencyID').value(),
      name: msdDoc.get('com:Name', prefixes).text(),
      structure: [],
    };

    // Report Structure
    const recStructures = (attrDocs) => {
      const result = [];

      attrDocs.forEach((attrDoc) => {
        const childAttrDocs = attrDoc.find('str:MetadataAttribute', prefixes);
        const childStructure = childAttrDocs.length > 0 ? recStructures(childAttrDocs) : [];
        const attribute = {
          id: attrDoc.attr('id').value(),
          label: attrDoc.attr('id').value(),
          structure: childStructure,
        };

        // local representation
        const representationDoc = attrDoc.get('str:LocalRepresentation', prefixes);
        const textFormatDoc = representationDoc.get('str:TextFormat', prefixes);
        const enumerationDoc = representationDoc.get('str:Enumeration/Ref', prefixes);

        if (textFormatDoc) {
          attribute.type = textFormatDoc.attr('textType').value();
        }
        else if (enumerationDoc) {
          attribute.type = 'enumeration';
          // attribute.codeList =
        }

        if (attribute.type !== undefined) {
          result.push(attribute);
        }
      });

      return result;
    };

    const reportStructure = msdDoc.get('str:MetadataStructureComponents/str:ReportStructure', prefixes);
    msd.structure = recStructures(reportStructure.find('str:MetadataAttribute', prefixes));

    list.push(msd);
  });

  return list;
};

module.exports = Metadata;
