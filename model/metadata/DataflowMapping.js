const mongoose = require('mongoose');

// Schema
const schema = new mongoose.Schema({
  dataflowId: { type: String, required: true },
  msdId: { type: String, required: true },
});

// Model
const model = module.exports = mongoose.model('dataflowmapping', schema);
